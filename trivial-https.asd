;;;; Silly emacs, this is -*- Lisp -*- (or thereabouts)

;;this is necessary due to a bug in SBCL
#+sbcl
(require :sb-bsd-sockets)

(defsystem trivial-https
    :name "trivial-https"
    :author "Brian Mastenbrook/David Lichteblau"
    :licence "MIT"
    :description "Trivial support for HTTP GET and POST."
    :depends-on (:trivial-sockets :cl+ssl)
    :components ((:file "trivial-https")))
